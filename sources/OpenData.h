/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  SR_team me@sr.team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENDATA_H
#define OPENDATA_H

#include <filesystem>
#include <map>

/**
 * @todo write docs
 */
class OpenData
{
    std::map<std::filesystem::path, std::filesystem::perms> orig;
    
public:
    /**
     * Default constructor
     */
    OpenData();
    
    ~OpenData();
    
protected:
    void grantAccess(const std::filesystem::path &path);

};

#endif // OPENDATA_H
