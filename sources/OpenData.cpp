/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  SR_team me@sr.team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpenData.h"

#include <dlfcn.h>

#include <string>
#include <string_view>

#include <android/log.h>

OpenData g_instance;

OpenData::OpenData()
{
    Dl_info info;
    dladdr((void*)&g_instance, &info);
    std::string_view fullPath(info.dli_fname);
    std::string pkg;
    if (fullPath.starts_with("/data/app")){
        // /data/app/package.name-HASH==/lib/ARCH/
        pkg = fullPath.substr(0, fullPath.rfind("==") - 23);
        pkg = pkg.substr(pkg.rfind('/') + 1);
    } else {
        // /data/data/package.name/lib/
        pkg = fullPath.substr(0, fullPath.rfind("/lib/"));
        pkg = pkg.substr(pkg.rfind('/') + 1);
    }
    grantAccess(std::filesystem::path("/data") / "data" / pkg);
}

OpenData::~OpenData()
{
    for (auto &&[path, perm] : orig ){
        try{
            std::filesystem::permissions(path, perm);
        } catch (std::filesystem::filesystem_error &e){
            __android_log_print(ANDROID_LOG_WARN, "OpenData", "Can't restore access to %s - %s", path.string().data(), e.what());
        }
    }
}

void OpenData::grantAccess(const std::filesystem::path &path){
    for ( auto &&file : std::filesystem::directory_iterator( path ) ){
        orig[file.path()] = std::filesystem::status(file.path()).permissions();
        try{
            std::filesystem::permissions(file.path(), std::filesystem::perms::owner_all | std::filesystem::perms::group_all | std::filesystem::perms::others_all);
        } catch (std::filesystem::filesystem_error &e){
            __android_log_print(ANDROID_LOG_WARN, "OpenData", "Can't grant access to %s - %s", file.path().string().data(), e.what());
        }
        if (file.is_directory() && !file.is_symlink())
            grantAccess(file.path());
    }
}
